import org.sql2o.*;
import org.junit.*;
import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.List;

public class StylistTest {

  @Rule
  public DatabaseRule database = new DatabaseRule();

  @Test
  public void Stylist_instantiatesCorrectly_true() {
    Stylist myStylist = new Stylist("Mark Danger");
    assertEquals(true, myStylist instanceof Stylist);
  }

  @Test
  public void getName_returnsName_String() {
    Stylist myStylist = new Stylist("Mark Danger");
    assertEquals("Mark Danger", myStylist.getName());
  }

  @Test
  public void all_emptyAtFirst_0() {
    assertEquals(Stylist.all().size(), 0);
  }

  @Test
  public void all_returnsAllStylists_0() {
    Stylist firstStylist = new Stylist("Mark Danger");
    Stylist secondStylist = new Stylist("Mary Danger");
    firstStylist.save();
    secondStylist.save();
    assertEquals(2, Stylist.all().size());
  }

  @Test
  public void equals_returnsTrueIfNamesAretheSame_true() {
    Stylist firstStylist = new Stylist("Mark Danger");
    Stylist secondStylist = new Stylist("Mark Danger");
    assertTrue(firstStylist.equals(secondStylist));
  }

  @Test
  public void save_savesIntoDatabase_true() {
    Stylist myStylist = new Stylist("Mark Danger");
    myStylist.save();
    assertTrue(Stylist.all().get(0).equals(myStylist));
  }

  @Test
  public void save_assignsIdToObject_int() {
    Stylist myStylist = new Stylist("Mark Danger");
    myStylist.save();
    Stylist savedStylist = Stylist.all().get(0);
    assertEquals(myStylist.getId(), savedStylist.getId());
  }

  @Test
  public void find_findStylistInDatabase_true() {
    Stylist myStylist = new Stylist("Mark Danger");
    myStylist.save();
    Stylist savedStylist = Stylist.find(myStylist.getId());
    assertTrue(myStylist.equals(savedStylist));
  }

  @Test
  public void getClients_retrievesALlClientsFromDatabase_clientsList() {
    Stylist myStylist = new Stylist("Mark Danger");
    myStylist.save();
    Client firstClient = new Client("John Doe", myStylist.getId());
    firstClient.save();
    Client secondClient = new Client("Jane Roe", myStylist.getId());
    secondClient.save();
    Client[] clients = new Client[] { firstClient, secondClient };
    assertTrue(myStylist.getClients().containsAll(Arrays.asList(clients)));
  }

}
