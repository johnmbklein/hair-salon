import java.util.Date;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import org.junit.*;
import static org.junit.Assert.*;
import org.sql2o.*;
import java.util.List;

public class ClientTest {

  @Rule
  public DatabaseRule database = new DatabaseRule();

  @Test
  public void client_instantiatesCorrectly_true() {
    Client myClient = new Client("John Doe", 1);
    assertEquals(true, myClient instanceof Client);
  }

  @Test
  public void getName_returnsName_String() {
    Client myClient = new Client("John Doe", 1);
    assertEquals("John Doe", myClient.getName());
  }

  @Test
  public void getStylistId_returnsStylistId_1() {
    Client myClient = new Client("John Doe", 1);
    myClient.save();
    assertEquals(1, myClient.getStylistId());
  }

  @Test
  public void all_emptyAtFirst_0() {
    assertEquals(Client.all().size(), 0);
  }

  @Test
  public void equals_returnsTrueIfNamesAretheSame_String() {
    Client firstClient = new Client("John Doe", 1);
    Client secondClient = new Client("John Doe", 1);
    assertTrue(firstClient.equals(secondClient));
  }

  @Test
  public void save_assignsIdToObject_id() {
    Client myClient = new Client("John Doe", 1);
    myClient.save();
    Client savedClient = Client.all().get(0);
    assertEquals(myClient.getId(), savedClient.getId());
  }

  @Test
  public void save_savesClientToDatabase_true() {
    Client myClient = new Client("John Doe", 1);
    myClient.save();
    assertTrue(Client.all().get(0).equals(myClient));
  }

  @Test
  public void find_findsClientInDatabase_true() {
    Client myClient = new Client("John Doe", 1);
    myClient.save();
    Client savedClient = Client.find(myClient.getId());
    assertTrue(myClient.equals(savedClient));
  }

  @Test
  public void find_returnsNullWhenNoClientFound_null() {
    assertTrue(Client.find(999) == null);
  }

}
