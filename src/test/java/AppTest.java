import org.fluentlenium.adapter.FluentTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import static org.assertj.core.api.Assertions.assertThat;
import static org.fluentlenium.core.filter.FilterConstructor.*;
import org.junit.*;
import java.util.List;
import org.sql2o.*;

public class AppTest extends FluentTest {
  public WebDriver webDriver = new HtmlUnitDriver();

  @Rule
  public DatabaseRule database = new DatabaseRule();

  @Override
  public WebDriver getDefaultDriver() {
    return webDriver;
  }

  @ClassRule
  public static ServerRule server = new ServerRule();

  @Test
  public void rootTest() {
    goTo("http://localhost:4567/");
    assertThat(pageSource()).contains("Mark Danger's Dope Cuts");
  }

  @Test
  public void stylistIsCreatedTest(){
    goTo("http://localhost:4567/");
    click("a", withText("Add a New Stylist"));
    fill("#stylist-name").with("Mark Danger");
    submit(".btn");
    assertThat(pageSource()).contains("Mark Danger");
  }

  @Test
  public void stylistIsDisplayedTest() {
    Stylist myStylist = new Stylist("Mark Danger");
    myStylist.save();
    String stylistPath = String.format("http://localhost:4567/stylists/%d", myStylist.getId());
    goTo(stylistPath);
    assertThat(pageSource()).contains("Mark Danger");
  }

  @Test
  public void stylistPageDisplaysName() {
    goTo("http://localhost:4567/stylists/new");
    fill("#stylist-name").with("Mark Danger");
    submit(".btn");
    click("a", withText("Mark Danger"));
    assertThat(pageSource()).contains("Mark Danger");
  }

  @Test
  public void stylistAddClientFormIsDisplayed() {
    goTo("http://localhost:4567/stylists/new");
    fill("#stylist-name").with("Mark Danger");
    submit(".btn");
    click("a", withText("Mark Danger"));
    click("a", withText("Add a new client"));
    assertThat(pageSource()).contains("Add a client to Mark Danger");
  }

  @Test
  public void allClientsDisplayOnStylistPage() {
    Stylist myStylist = new Stylist("Mark Danger");
    myStylist.save();
    Client firstClient = new Client("Matt Danger", myStylist.getId());
    firstClient.save();
    Client secondClient = new Client("Mary Danger", myStylist.getId());
    secondClient.save();
    String categoryPath = String.format("http://localhost:4567/stylists/%d", myStylist.getId());
    goTo(categoryPath);
    assertThat(pageSource()).contains("Matt Danger");
    assertThat(pageSource()).contains("Mary Danger");
 }

  @Test
  public void clientIsAddedAndDisplayed() {
    goTo("http://localhost:4567/stylists/new");
    fill("#stylist-name").with("Mark Danger");
    submit(".btn");
    click("a", withText("Mark Danger"));
    click("a", withText("Add a new client"));
    fill("#client-name").with("Mary Danger");
    submit(".btn");
    assertThat(pageSource()).contains("Mary Danger");
  }

  @Test
  public void clientNotFoundMessageShown() {
    goTo("http://localhost:4567/clients/999");
    assertThat(pageSource()).contains("Client not found");
  }

  @Test
  public void clientsPageShowsAllClients() {
    Client myClient = new Client("Mary Danger", 1);
    myClient.save();
    goTo("http://localhost:4567/clients");
    assertThat(pageSource()).contains("Mary Danger");
  }

  // I can't seem to get this to work with a select box. Suggestions?
  // @Test
  // public void newClientPageAddsClient() {
  //   Stylist newStylist = new Stylist("Mark Danger");
  //   goTo("http://localhost:4567/clients/new");
  //   fill("#client-name").with("Mary Danger");
  //   click("option", withText("Mark Danger"));
  //   submit(".btn");
  //   String path = String.format("/stylists/%d", newStylist.getId());
  //   goTo("path");
  //   assertThat(pageSource()).contains("Mary Danger");
  // }

}
