# Hair Salon

#### App to manage clients and stylists for a hair salon

#### By John Klein

## Description

A management tool that enables hair salons to manage clients and stylists.

## Known Bugs

None.

## Support and contact details

Get in touch with my with questions or comments or if you find a bug.

## Technologies Used

This page was built using Java, HTML, Velocity, and Spark. Testing was done using JUnit and FluentLenium. Database uses Postgres.

### License

Licensed under GPL. See license file for more information.

Copyright (c) 2016 John Klein
